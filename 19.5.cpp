﻿#include <iostream>

class Animal {
public:
    virtual void Voice() {
        std::cout << "Animal voice" << std::endl;
    }
};

class Dog : public Animal {
public:
    void Voice() override {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        std::cout << "Meow!" << std::endl;
    }
};

class Cow : public Animal {
public:
    void Voice() override {
        std::cout << "Moo!" << std::endl;
    }
};

int main() {
    Animal* animals[] = { new Dog(), new Cat(), new Cow(), new Dog(), new Cat(), new Cow() };

    for (Animal* animal : animals) {
        animal->Voice();
    }

    for (Animal* animal : animals) {
        delete animal;
    }

    return 0;
}
